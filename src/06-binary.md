# Binary session

Slides: [Binary](https://docs.google.com/presentation/d/1X0-KC8g9pS6WziwQvgzaklLOTgOdymILdbgD1i-73eo/edit?usp=sharing)

## Intro

Binary challenges are some of the hardest challenges to get into learning how
to do, however, they're massively rewarding, and allow learning about all
aspects of operating systems by solving them.

Essentially, a binary challenge will present you with a runnable program. In
a reversing challenge, you have to work out how the program works, and
extract the hidden flag somehow, in a pwn challenge, you have to work out how
to find and exploit a bug in the challenge to be able to read the flag.

## Conventions

It's worth mentioning a couple of the conventions in these challenges, since
they'll be quite useful in solving them.

Reversing:

- You'll get a binary, that will generally either:
  1. Print out the flag (under the correct conditionss
  2. Allow you to guess the flag (and tell you if you're correct)
- The goal is to either:
  1. Work out the conditions that print out the flag (and cause them to
     occur, or skip over them)
  2. Work out how the flag is checked, and derive it yourself.

Pwn:

- You'll get a binary identical to one running on the server (maybe with the flag redacted)
- The challenge will either be impossible or incredibly impractical to solve
  in the "normal" way
- The usual goal is to get arbitrary code execution, and use that to read the
  flag file (usually flag.txt)
- To solve the challenge, you can analyze the binary locally, and then attack
  the version on the server
  - Sometimes there may be *very* subtle differences


## Exploitation

Footholds:

- Buffer overflows
  - Usually from an insecure function, `gets`, `strcpy`, etc (but can also be
    more subtle)
  - Targets:
    - Interesting buffers
    - Function pointers
    - Saved instruction pointer
- Format string attack
  - Passing a user-controlled string into the format argument of `printf`
  - Can dump the stack very easily
  - Can usually construct an arbitrary-read/write with more difficulty
  - Targets:
    - Memory leaking addresses
    - Writing to the GOT

Protections:

- NX bit (can't execute the stack/heap)
  - Overcome using a ROP/ROJ attack
- Stack cookies/canaries (protects overflow of return instruction pointer)
  - Value is saved at the end of the stack, and before returning, it's
    checked to see if it's been changed
  - The value is the same for *all* functions and copied to forked processes
  - Usually overcome by leaking the canary first
- ASLR (base addresses of the stack/heap are randomized)
  - **Note**: this is a feature of the system, not to the binary
  - Overcome by getting a memory leak of some known address on the
    stack/heap
  - A traditional technique is to leak something, then jump back to the
    start of main
- PIE (locations of code are also randomized)
  - Overcome by leaking the saved instruction pointer
  - Even if the target binary doesn't have PIE, it's very likely that libc
    will have it (and will be loaded at an arbitrary address)

## Hints

Reversing:
- Be patient - reversing is fiddly.
- Look for functions with interesting names
- If the binary is stripped, **name/type variables and functions as you
  analyze**.
  - This will help you understand what's going on, and be useful when you
  come back to it
- Use a variety of different tools and techniques, different ones have
  different strengths
  - e.g. ghidra has an excellent decompiler, radare has an excellent debugger
- Work out where the flag is
  - Is it encrypted? If so, how?
  - Is it fetched from a server? If so, under which conditions?
  - Is it assembled bit by bit? If so, from where?
- Learn to recognize common patterns, and make assumptions about how things
  are working (but don't gloss over the details).

Pwn:
- On *very* easy challenges, you might be given a function to target, or a
  very clearly defined goal.
- You might also get source code! Look at it, but don't trust it, compilation
  may drastically modify how it works.
- Look for always-vulnerable functions first, like `strcpy` or `gets`.
- Look for sometimes-vulnerable functions like `printf` (and variants).
- Look for vulnerable patterns:
  - Trusting user-provided lengths
  - Using a predictable random number generator
- Failing all else, reverse-engineer until you get somewhere
- For large programs, you might want to look into static
  analysis/fuzzing/symbolic execution techniques.

## Demo

For the demo, we look at a challenge from this year's mini-CTF at
HackTheMidlands.

[htm-sh](https://drive.google.com/file/d/1V4uoqAO1ORgaFuOY9cRdA1bF95ZAEenE/view?usp=sharing)

The source code for the challenge is located [on
GitHub](https://github.com/jedevc/HackTheMidlandsCTF20/tree/master/challenges/7-bonus-auth),
but shouldn't be used for solving the challenge.

## Challenges

## 0 - Demo

If you're not confident, take a look at the demo yourself, and poke around
with it using your tool of choice.

Look for:

- What sections are there? Look them up, what are they all for?
- What defensive protections does the program have? How do they work?
- Which different ways can you look at a binary?
  - Try out: hexdump view, disassembly listing, graph view, decompiled view
- How does each function work individually? Look at the patterns in them.

## 1 - OverTheWire Narnia

Once you've understood the basics, OverTheWire has some really good
introductory pwn challenges with source code in
[Narnia](https://overthewire.org/wargames/narnia/). These are excellent for
learning the absolute basics, and start with simple buffer overflows and
format string vulnerabilities and get you to explore a variety of different
cases.

Narnia works a little different from other pwn challenges, where you have a
binary running on the server; in these wargames you typically ssh into a box,
and have direct access to the binary to exploit. This binary is usually set
with the SUID or GUID bit which means that instead of running as the
user/group who ran it, it will run as the user/group who owns it. If you can
exploit the program, then you'll be able to run things as this user (and read
the stored password in /etc/narnia_pass)!

## 2 - HackTheBox

We're gonna have a go at the HackTheBox challenges. They have two different
sections:

- [Reversing](https://www.hackthebox.eu/home/challenges/Reversing)
- [Pwn](https://www.hackthebox.eu/home/challenges/Pwn)

Do the easy ones first (or jump right into the hard ones... if you want)!
