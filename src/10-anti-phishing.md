# AFNOM ANTI-PHISHING 

## Ideas:

 - Crawl registered domains / [cert.sh](https://github.com/snwlol/crt.sh) to look for suspicious domains
 - play with JA3s signatures
 - Develop / use automated tools to analyse sites for common parameters
 - Discover attacker infrastructure using OSINT tooling
 - Develop markers and signatures for identifying suspicious sites
 - Create automated methods of querying site reputation

## Big list of other tools:

* [Lenny Zeltser's List for Online Tools for Looking up Potentially Malicious Websites](https://zeltser.com/lookup-malicious-websites/)

## Discovery:

Finding sites using lists of registered domains:

* [Evilblog -- Catching phishing sites with certstream logs](https://securityevil.blogspot.com/2017/12/catching-phishing-sites-with-certstream_26.html)
* [Execute Malware Blog -- Finding Phishing Websites](https://executemalware.com/?p=258)


## Analysis:

### URL reputation analysis:
* [https://scanurl.net/](https://scanurl.net/)
* [https://sitecheck.sucuri.net/](https://sitecheck.sucuri.net/)

### Auto malware analysis:
* [https://any.run/](https://any.run/)
* [https://www.hybrid-analysis.com/](https://www.hybrid-analysis.com/)
* [https://www.websense.com/content/support/library/email/v85/email_help/url_sandboxing_explain_esg.aspx](https://www.websense.com/content/support/library/email/v85/email_help/url_sandboxing_explain_esg.aspx)

### Automatically get pictures from sites:
* [https://github.com/maaaaz/webscreenshot](https://github.com/maaaaz/webscreenshot)


### Analyse content:
* [https://github.com/kurobeats/wordhound](https://github.com/kurobeats/wordhound)
* [https://github.com/HackLikeAPornstar/StratJumbo/blob/master/chap3/wordcollector.py](https://github.com/HackLikeAPornstar/StratJumbo/blob/master/chap3/wordcollector.py)
* [https://www.autopilothq.com/blog/email-spam-trigger-words/](https://www.autopilothq.com/blog/email-spam-trigger-words/)
* [https://prospect.io/blog/455-email-spam-trigger-words-avoid-2018/](https://prospect.io/blog/455-email-spam-trigger-words-avoid-2018/)

## OSINT

### Backlink checker (very cool)

* [https://ahrefs.com/backlink-checker](https://ahrefs.com/backlink-checker)

### WHOIS
* [https://whois.net/](https://whois.net/)
* [https://raw.githubusercontent.com/HackLikeAPornstar/StratJumbo/master/chap1/query_whois.py](https://raw.githubusercontent.com/HackLikeAPornstar/StratJumbo/master/chap1/query_whois.py)

### Infrastructure tracking
* [https://censys.io/](https://censys.io/)

### Domain ownership & History:
* [https://securitytrails.com/](https://securitytrails.com/)

### Blogs
* [https://www.maltego.com/blog/tracking-typosquatting-and-brand-monitoring-with-maltego/](https://www.maltego.com/blog/tracking-typosquatting-and-brand-monitoring-with-maltego/)
* [https://www.maltego.com/blog/using-maltego-to-hunt-for-phishing-subdomains/](https://www.maltego.com/blog/using-maltego-to-hunt-for-phishing-subdomains/)

### Free-er version of maltego:
* [https://www.spiderfoot.net/](https://www.spiderfoot.net/)

## Reporting:

### Emails:
* [https://www.phishing.org/how-to-report-phishing](https://www.phishing.org/how-to-report-phishing)
* [https://www.ncsc.gov.uk/information/report-suspicious-emails](https://www.ncsc.gov.uk/information/report-suspicious-emails)

### Sites:
* [https://www.gov.uk/report-suspicious-emails-websites-phishing](https://www.gov.uk/report-suspicious-emails-websites-phishing)
* [https://safebrowsing.google.com/safebrowsing/report_phish/?hl=en](https://safebrowsing.google.com/safebrowsing/report_phish/?hl=en)
* [https://www.kaspersky.co.uk/resource-center/preemptive-safety/how-to-report-a-website](https://www.kaspersky.co.uk/resource-center/preemptive-safety/how-to-report-a-website)

## Misc:

* [http://checkshorturl.com/](http://checkshorturl.com/)
* [https://gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)
* [Wireshark](https://www.wireshark.org/)
