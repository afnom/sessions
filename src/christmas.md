# Christmas

This week, we've all built our own challenges, and gifted them to each other
to solve for our Christmas session!

## Santa's Secrets (@leet_lemon)

Santa's been using a new password manager made by one of his elf interns!
It's been very useful for him, he doesn't have to write down all the security
codes for his North Pole factories anymore, which means he doesn't need to
use as much paper, and is better for the environment.

But he's forgotten the password, and can't remember how to get in... Any
chance you can work out how to open and decrypt his database?

```
Q1JQV2h0dHBzOi8vcGFzdGViaW4uY29tL1N6UE0wbTYyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAABzYW50YQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAACqjezcUMowhgewDEn0xL1uYr/5bvX4ue6Kn/er9q4hGT09gL/V2
/CZpOMUXMeZyQxJpC2xF1I73r60nRQ6Ht6RUzBQwJ/69aQifNtF4MP7q+TKZNg4nhzU2kEpp8+9j
VMjGVHc2lp5UveDDbOAnRUEh6qRUAK1jJ/7wAzufNgUSY/7qLcvMNg5aIWg2kH0DJ+9jh2L5VHdp
MNFUvRRdn+AneNpU6qSHmeBjJzKKNjufaZ5FY/4exv7MNkHzVGg2wxc2J++WIZX5VKoDY9FU8K2Q
n+BaEg5U6tchzOBjWsu9NjvSA9FFYzK3+f7MadonVGhpXUo2JyMwVJX5h0Q2Y9GHiuCQnxTzRQ5U
HnFUzOCW8/69Nm5sNtFFlsvq+f4AAw4nVJsDkEo2WrxjVJUtIXc2YwUhveCQ0q0nRQ6Ht6RUzBQw
J/69aQifNtF4MP7q+TKZNg4nhzU2kEpp8+9jVMjGVHc2lp5UveDDbOAnRUEh6qRUAK1jJ/7wAzuf
NgUSY/7qLcvMNg5aIWg2kH0DJ+9jh2L5VHdpMNFUvRRdn+AneNpU6qSHmeBjJzKKNjufaZ5FY/4e
xv7MNkHzVGg2wxc2J++WIZX5VKoDY9FU8K2Qn+BaEg5U6tchzOBjWsu9NjvSA9FFYzK3+f7Madon
VGhpXUo2JyMwVJX5h0Q2Y9GHiuCQnxTzRQ5UHnFUzOCW8/69Nm5sNtFFlsvq+f4AAw4nVJsDkEo2
WrxjVJUtIXc2YwUhveCQ0q0nRQ6Ht6RUzBQwJ/69aQifNtF4MP7q+TKZNg4nhzU2kEpp8+9jVMjG
VHc2lp5UveDDbOAnRUEh6qRUAK1jJ/7wAzufNgUSY/7qLcvMNg5aIWg2kH0DJ+9jh2L5VHdpMNFU
vRRdn+AneNpU6qSHmeBjJzKKNjufaZ5FY/4exv7MNkHzVGg2wxc2J++WIZX5VKoDY9FU8K2Qn+Ba
Eg5U6tchzOBjWsu9NjvSA9FFYzK3+f7MadonVGhpXUo2JyMwVJX5h0Q2Y9GHiuCQnxTzRQ5UHnFU
zOCW8/69Nm5sNtFFlsvq+f4AAw4nVJsDkEo2WrxjVJUtIXc2YwUhveCQ0q0nRQ6Ht6RUzBQwJ/69
aQifNtF4MP7q+TKZNg4nhzU2kEpp8+9jVMjGVHc2lp5UveDDbOAnRUEh6qRUAK1jJ/7wAzufNgUS
Y/7qLcvMNg5aIWg2kH0DJ+9jh2L5VHdpMNFUvRRdn+AneNpU6qSHmeBjJzKKNjufaZ5FY/4exv7M
NkHzVGg2wxc2J++WIZX5VKoDY9FU8K2Qn+BaEg5U6tchzOBjWsu9NjvSA9FFYzK3+f7MadonVGhp
XUo2JyMwVJX5h0Q2Y9GHiuCQnxTzRQ5UHnFUzOCW8/69Nm5sNtFFlsvq+f4AAw4n
```


## THE PEAR TREE (@hendo)

Can you figure out the secret of the magical pear tree?

Download the zip archive from: https://github.com/Jrhenderson11/THE_PEAR_TREE

## The Elves ([@raine](https://github.com/rainestormee))

Santa's Elves have lost a flag! They tried their hardest to write a script to retrieve it, but they need **your** help to finish it off!

Starting point is available from [here](https://drive.google.com/drive/folders/1hdD4jtBKboJkC3kCEhmRs8RPiK2HqVWl?usp=sharing)

## Hack Hanukkah (@heavyimage)

Our Hanukkah website seems to have broken this year and the programmer who built it using some non-standard technolgies is AWOL!  Can you fix the site by relighting all 8 candles and saving the holiday?!

To help, run a local copy of the site with docker: `docker run -d --rm heavyimage/hack_hanukkah`.  Once the image is running, the site should be visible [here](http://172.17.0.2:5000).

## Sneaky Elves (@zerobadgers)

Santa won't let his elves email each other at work because emails distract them from making toys. So instead they use cats.
Can you find out what Figgy sent Fozie?

https://we.tl/t-DIAj26qdRY 

## Santa's Reindeers

Challenge description:

<img src="files/xmas20.pre.png" width=300 />

Challenge (non-standard flag format):

<img src="files/xmas20.chall.png" width=300 />
